/////////////////
//  VARIABLES  //
/////////////////

const rand_max = 10;
const no = 9;
let pp = [];
let w8_t = 0;
let ta_t = 0;


/////////////////
//  FUNCTIONS  //
/////////////////

Process = function (nr, max = 1, service_time = 0, turnaround_time = 0) {
	this.nr = nr;
	this.execute_time = Math.floor(Math.random() * max) + 1;
	this.service_time = service_time;
	this.turnaround_time = turnaround_time;
}

function avgWaitingTime (tab) {
	let res = 0
	for (var i = 0; i < tab.length; i++) {
		res += tab[i].service_time
	};
	return res / tab.length;
}

function avgTurnaroundTime (tab) {
	let res = 0
	for (var i = 0; i < tab.length; i++) {
		res += tab[i].turnaround_time
	};
	return res / tab.length;
}


function addProcess(){
	if (pp == 0) {
		pp.push(new Process(pp.length, rand_max))
	} else{
		w8_t += pp[pp.length-1].execute_time;
		pp.push(new Process(pp.length, rand_max, w8_t, ta_t))
	};
	pp[pp.length-1].turnaround_time = pp[pp.length-1].execute_time + pp[pp.length-1].service_time;
}

function run(no, rand_max){
	for (let i = 0; i < no; i++) {
		if (i === 0) {
			pp.push(new Process(i, rand_max))
		} else{
			w8_t += pp[i-1].execute_time;
			pp.push(new Process(i, rand_max, w8_t, ta_t))
		};
		pp[i].turnaround_time = pp[i].execute_time + pp[i].service_time;
	};
}

function consoleDisplay() {
	console.table(pp);
	console.log("Avg waiting_time : ", avgWaitingTime(pp).toFixed(2));
	console.log("Avg turnaround time : ", avgTurnaroundTime(pp).toFixed(2));
}


///////////
//  GET  //
///////////

var getLastProcess = () => { return pp[pp.length-1]; }
var getAvgWaitingTime = () => { return avgWaitingTime(pp).toFixed(2); }
var getAvgTurnaroundTime = () => { return avgTurnaroundTime(pp).toFixed(2); }
